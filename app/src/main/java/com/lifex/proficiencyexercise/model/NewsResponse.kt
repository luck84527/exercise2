package com.lifex.proficiencyexercise.model

import com.google.gson.annotations.SerializedName

data class NewsResponse(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("rows")
	val rows: ArrayList<RowsItem> = ArrayList()
)

data class RowsItem(

	@field:SerializedName("imageHref")
	val imageHref: String? = "",

	@field:SerializedName("description")
	val description: String? = "",

	@field:SerializedName("title")
	val title: String? = ""
)


