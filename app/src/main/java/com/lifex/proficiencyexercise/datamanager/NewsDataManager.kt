package com.lifex.proficiencyexercise.datamanager

import android.app.Activity
import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.lifex.proficiencyexercise.R
import com.lifex.proficiencyexercise.model.NewsResponse
import org.json.JSONObject
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException

class NewsDataManager constructor(activity: Context) {

    private val inputStream = activity.resources.openRawResource(R.raw.dummy_data)
    private val outputStream = ByteArrayOutputStream()


    fun readNewsData(): NewsResponse? {
        var reader: Int
        try {
            reader = inputStream.read()
            while (reader != -1) {
                outputStream.write(reader)
                reader = inputStream.read()
            }
            inputStream.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return try {
            Gson().fromJson(outputStream.toString(), NewsResponse::class.java)
        } catch (e: Exception) {
            null
        }


    }


}
