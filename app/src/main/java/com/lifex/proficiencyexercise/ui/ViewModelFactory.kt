package com.lifex.proficiencyexercise.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lifex.proficiencyexercise.App
import com.lifex.proficiencyexercise.datamanager.NewsDataManager


class ViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            return MainActivityViewModel(newsDataManager = NewsDataManager(context)) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}