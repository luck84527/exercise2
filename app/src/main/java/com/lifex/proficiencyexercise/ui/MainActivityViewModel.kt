package com.lifex.proficiencyexercise.ui

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lifex.proficiencyexercise.datamanager.NewsDataManager
import com.lifex.proficiencyexercise.model.NewsResponse

class MainActivityViewModel(private val newsDataManager: NewsDataManager) : ViewModel() {


    private val _newsResponse = MutableLiveData<NewsResponse>()
    val newsResponse: LiveData<NewsResponse> = _newsResponse


    fun getNews() {
        _newsResponse.value = newsDataManager.readNewsData()
    }


}