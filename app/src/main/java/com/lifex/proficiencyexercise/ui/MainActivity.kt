package com.lifex.proficiencyexercise.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.lifex.proficiencyexercise.R
import com.lifex.proficiencyexercise.adapter.NewsAdapter
import com.lifex.proficiencyexercise.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    lateinit var binding: ActivityMainBinding
    private val rvNews: RecyclerView by lazy { findViewById(R.id.rvNews) }

    //Variables initialized
    lateinit var loginViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        loginViewModel = ViewModelProvider(this,ViewModelFactory(this)).get(MainActivityViewModel::class.java)
        loginViewModel.getNews()
        loadLocalData()

    }

    private fun loadLocalData() {
        loginViewModel.newsResponse.observe(this, Observer {
            it?.let { newsList ->
                binding.rvNews.adapter = NewsAdapter(this, newsList.rows)
            }
        })
    }

}