package com.lifex.proficiencyexercise.utils

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.lifex.proficiencyexercise.R


object BindingAdapters {
    @BindingAdapter("app:glideSrc")
    @JvmStatic
    fun glideSrc(view: ImageView, image: String?) {
        Glide.with(view.context).load(image).placeholder(R.drawable.ic_baseline_image_24).into(view)
    }
}
